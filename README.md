# Transpanight

Tema Oscuro Transparente para la Extension de navegador Tabliss

<img src="Capturas/Captura_de_pantalla.png">

## Como Agregar Este tema a Tabliss?

Abra una nueva Pestaña, arriba hacia la izquierda de click sobre el engranaje

<img src="Capturas/1.png" width="500">

Dirijase al apartado de "Widgets", y seleccione "Custom CSS"

<img src="Capturas/2.png" width="500">.


<img src="Capturas/3.png" width="500">

Ahi mismo en "Widgets", de click sobre el engranaje de "Custom CSS"

<img src="Capturas/4.png" width="500">

Copie el codigo o estilos del "Transpanight.css" a ese cuadro blanco, y... listo.

<img src="Capturas/5.png" width="500">
